package org.test;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.client.widgets.Grid;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.NativeSelect;




import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;


/**
 *
 */
@Theme("mytheme")
@Widgetset("org.test.MyAppWidgetset")
public class MyUI<Person> extends UI {


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        //ADDED BY ME
        // Create a text field
        TextField name1 = new TextField("Hello and welcome on this website, please type your first name");

        // Put some initial content in it
        name1.setValue("First name");


        //<-ADDED BY ME

        final VerticalLayout layout = new VerticalLayout();
        
        final TextField name2 = new TextField();
        name2.setCaption("Now please type your last name:");
        name2.setValue("Last name");


        // Create a DateField with the default style
        DateField date = new DateField();
        date.setCaption("Enter your birth's date");

        CheckBox checkbox1 = new CheckBox("Check the box if you liked this service");
        checkbox1.addValueChangeListener(event ->
                layout.addComponent(new Label("Thanks! Glad you liked it.")));

        Button button = new Button("Enter");
        button.addClickListener( e -> {
            layout.addComponent(new Label("Thanks " + name1.getValue() + " "+ name2.getValue()
                    + ", everything is in order!" ));
            layout.addComponent(checkbox1);
        });



        layout.addComponents(name1, name2, date, button);

        layout.setMargin(true);
        layout.setSpacing(true);
        
        setContent(layout);


    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
